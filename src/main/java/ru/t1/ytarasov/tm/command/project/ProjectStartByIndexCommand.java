package ru.t1.ytarasov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start project by index";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        @Nullable final String userId = getAuthService().getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
