package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.Domain;
import ru.t1.ytarasov.tm.exception.AbstractException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

public final class DataXmlJaxbLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-xml-jaxb-load";

    @NotNull
    public static final String DESCRIPTION = "Loads from xml by JAXB";

    @SneakyThrows
    @Override
    public void execute() throws AbstractException, IOException, ClassNotFoundException {
        System.out.println("[XML JAXB LOAD]");
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
