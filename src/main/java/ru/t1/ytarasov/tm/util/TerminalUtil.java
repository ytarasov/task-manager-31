package ru.t1.ytarasov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    static int nextNumber() throws IntegerInvalidException {
        try {
            @NotNull final String value = SCANNER.nextLine();
            return Integer.parseInt(value);
        }
        catch (final Exception e) {
            throw new IntegerInvalidException();
        }
    }

    final class IntegerInvalidException extends AbstractException {

        public IntegerInvalidException() {
            super("Value is not Integer");
        }

    }

}
