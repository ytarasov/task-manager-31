package ru.t1.ytarasov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.command.data.AbstractDataCommand;
import ru.t1.ytarasov.tm.command.data.DataBackupLoadCommand;
import ru.t1.ytarasov.tm.command.data.DataBackupSaveCommand;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup extends Thread {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void save() {
        bootstrap.runCommand(DataBackupSaveCommand.NAME, false);
    }

    @SneakyThrows
    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.runCommand(DataBackupLoadCommand.NAME, false);
    }

}
